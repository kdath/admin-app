import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store'
import { router } from './router'

Vue.config.productionTip = false

export const bus = new Vue()

new Vue({
  el: '#app',
  store,
  router,
  created () {
    this.$store.dispatch('initializeStore')
  },
  render: h => h(App)
}).$mount('#app')
