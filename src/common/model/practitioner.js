export class Practitioner {
  constructor (object) {
    this.id = ''
    this.authId = ''
    this.active = false // Aktiv
    this.json = this.template()
    this.firstName = ''
    this.lastName = ''
    this.name = this.firstName + ' ' + this.lastName
    this.listOfRetrieved = []
    this.listOfAdded = []
    this.listOfRemoved = []
    this.connected = []
    this.hasInitializedConnected = false
  }

  fromJson (json) {
    this.json = json
    if (this.json.identifier) {
      this.authId = (this.json.identifier[0].value) ? this.json.identifier[0].value : undefined
      if (Array.isArray(this.json.identifier) && this.json.identifier[1] !== undefined) {
        this.authId = (this.json.identifier[1].value) ? this.json.identifier[1].value : undefined
      }
    }
    this.active = (this.json.active) ? this.json.active : undefined
    if (this.json.name) {
      if (Array.isArray(this.json.name[0].given) && this.json.name[0].given.length > 0) {
        this.json.name[0].given.forEach((name) => {
          this.firstName += name + ' '
        })
      }
      this.firstName = this.firstName.substring(0, this.firstName.length - 1)
      this.lastName = (this.json.name[0].family) ? this.json.name[0].family : undefined
      this.name = this.firstName + ' ' + this.lastName
    }
    if (this.json.id) {
      this.id = (this.json.id) ? this.json.id : undefined
    }
  }

  setConnected () {
    let returnList = []
    if (this.listOfRetrieved !== undefined) {
      returnList = this.listOfRetrieved
    }
    if (this.listOfAdded !== undefined) {
      for (var i = 0; i < this.listOfAdded.length; i++) {
        returnList.push(this.listOfAdded[i])
      }
    }
    returnList = returnList.filter(item => {
      return !this.listOfRemoved.includes(item)
    })
    this.connected = returnList
  }

  resetChanges () {
    this.listOfAdded = []
    this.listOfRemoved = []
    this.connected = this.listOfRetrieved
  }

  completeChanges () {
    this.listOfAdded = []
    this.listOfRemoved = []
    this.connected = this.listOfRetrieved
  }

  fromObject (object) {
    this.authId = object.authId
    this.id = object.id
    this.firstName = object.firstName
    this.lastName = object.lastName
    this.active = object.active
    this.name = this.firstName + ' ' + this.lastName
    this.updateJson()
  }

  updateJson () {
    this.json.identifier = this.json.identifier ||
          [{ 'type': {
            'coding': [
              {
                'system': 'http://hl7.org/fhir/ValueSet/identifier-type',
                'code': 'MD',
                'display': 'Medical License number'
              }
            ]
          },
          'system': 'https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/',
          'value': ''
          }]
    this.json.identifier[0].value = this.authId

    this.json.name = this.json.name || [{ family: undefined, given: [] }]
    this.json.name[0].family = this.lastName
    if (this.firstName) {
      this.firstName = this.firstName.trim()
      let nameArray = this.firstName.split(' ')
      this.json.name[0].given = nameArray
    }

    this.json.active = this.active
  }

  template () {
    return {
      'resourceType': 'Practitioner',
      'identifier': [
        {
          'type': {
            'coding': [
              {
                'system': 'http://hl7.org/fhir/ValueSet/identifier-type',
                'code': 'MD',
                'display': 'Medical License number'
              }
            ]
          },
          'system': 'https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/',
          'value': '' // Autorisations Id
        }
      ],
      'active': true,
      'name': [
        {
          'use': 'official',
          'family': '', // efternavn
          'given': [
            '' // for og mellemnavne
          ]
        }
      ]
    }
  }
}
