export class Parameters {
  constructor (object) {
    this.json = this.template()
    this.practitionerAutorisationsId = ''
    this.organizationSOR = ''
    this.xml = ''
  }

  fromJson (json) {
    this.json = json
    this.organizationSOR = (this.json.parameter[0].valueString) ? this.json.parameter[0].valueString : undefined
    this.practitionerAutorisationsId = (this.json.parameter[1].valueString) ? this.json.parameter[1].valueString : undefined
  }

  updateJson () {
    this.json.parameter[0].valueString = 'urn:oid:1.2.208.176.1.1|' + this.organizationSOR
    this.json.parameter[1].valueString = 'https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/|' + this.practitionerAutorisationsId
  }

  createXml () {
    this.xml = '<Parameters xmlns="http://hl7.org/fhir"><parameter><name value="organizationIdentifier"/><valueString value="urn:oid:1.2.208.176.1.1|' + this.organizationSOR + '"/></parameter><parameter><name value="practitionerIdentifier"/><valueString value="https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/|' + this.practitionerAutorisationsId + '"/></parameter></Parameters>'
  }

  template () {
    return {
      'resourceType': 'Parameters',
      'parameter': [
        {
          'name': 'organizationIdentifier',
          'valueString': '' // ID of the organization
        },
        {
          'name': 'practitionerIdentifier',
          'valueString': '' // ID of the practitioner
        }
      ]
    }
  }
}
