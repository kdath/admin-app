export class Organization {
  constructor (object) {
    this.id = ''
    this.sor = '' // SOR-kode
    this.json = this.template()
    this.name = ''
    this.alias = ''
    this.parrentSor = '' // Parrent SOR-kode
    this.listOfRetrieved = []
    this.listOfAdded = []
    this.listOfRemoved = []
    this.connected = []
    this.hasInitializedConnected = false
    this.oldAlias = ''
  }

  fromJson (json) {
    this.json = json
    if (this.json.identifier) {
      this.sor = (this.json.identifier[0].value) ? this.json.identifier[0].value : undefined
    }
    if (this.json.id) {
      this.id = (this.json.id) ? this.json.id : undefined
    }
    if (this.json.name) {
      this.name = this.json.name
    }
    if (this.json.alias) {
      this.alias = this.json.alias[0]
      this.oldAlias = this.alias
    }
    if (this.json.partOf) {
      this.parrentSor = (this.json.partOf.identifier.value) ? this.json.partOf.identifier.value : undefined
    }
  }

  setConnected () {
    let returnList = [...this.listOfRetrieved,
      ...this.listOfAdded]
    // if (this.listOfRetrieved !== undefined) {
    //   for (let i = 0; i < this.listOfRetrieved.length; i++) {
    //     returnList.push(this.listOfRetrieved[i])
    //   }
    // }
    // if (this.listOfAdded !== undefined) {
    //   for (let j = 0; j < this.listOfAdded.length; j++) {
    //     returnList.push(this.listOfAdded[j])
    //   }
    // }
    returnList = returnList.filter(item => {
      return !this.listOfRemoved.includes(item)
    })
    this.connected = returnList
  }

  resetChanges () {
    this.listOfAdded = []
    this.listOfRemoved = []
    this.connected = this.listOfRetrieved
    this.alias = this.oldAlias
  }

  completeChanges () {
    this.listOfAdded = []
    this.listOfRemoved = []
    this.connected = this.listOfRetrieved
    this.oldAlias = this.alias
  }

  setAlias (alias) {
    this.alias = alias
    this.updateJson()
  }

  updateJson () {
    this.json.identifier = this.json.identifier ||
          [{ system: 'urn:oid:1.2.208.176.1.1', value: undefined }]
    this.json.identifier[0].value = this.sor

    this.json.name = this.name

    this.json.alias = this.alias
  }

  template () {
    return {
      'resourceType': 'Organization',
      'id': '',
      'identifier': [
        {
          'system': 'urn:oid:1.2.208.176.1.1',
          'value': '' // SOR-kode
        }
      ],
      'type': [
        {
          'coding': [
            {
              'system': 'http://sor.sundhedsstyrelsen.dsdn.dk/lookupdata/#EntityTypes',
              'code': 'klinisk enhed',
              'display': 'Klinisk enhed'
            }
          ],
          'text': 'Klinisk enhed'
        }
      ],
      'active': true,
      'name': '', // navn
      'telecom': [
        {
          'system': 'phone',
          'value': '', // tlf nummer
          'use': 'work',
          'period': {
            'start': '2012-03-15'
          }
        }
      ],
      'address': [
        {
          'use': 'work',
          'type': 'postal',
          'line': [
            '', // navn på adresse fx. Aarhus universitetshospital
            '' // vejnav + nummer
          ],
          'city': '', // by
          'postalCode': '', // postnummer
          'country': '', // landkode: DNK
          'period': {
            'start': '2012-03-15'
          }
        }
      ],
      'alias': '', // alias
      'partOf': {
        'identifier': { // parrent SOR identifier
          'system': 'urn:oid:1.2.208.176.1.1',
          'value': ''
        },
        'type': 'Organization'
      }
    }
  }
}
