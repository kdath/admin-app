export class ExecuteRequest {
  constructor (object) {
    this.id = ''
    this.resource = undefined
    this.requestType = ''
    this.resourceType = ''
  }

  execute (fhir) {
    const query = {
      type: this.resourceType,
      id: this.id,
      data: this.resource.json,
      credentials: 'same-origin'
    }
    return new Promise((resolve, reject) => {
      if (this.requestType === 'update') {
        fhir.update(query).then(response =>
          console.log('request complete: ' + response)
        )
      }
      if (this.requestType === 'create') {
        fhir.create(query).then(response =>
          console.log('request complete: ' + response)
        )
      }
      if (this.requestType === 'delete') {
        fhir.delete(query).then(response =>
          console.log('request complete: ' + response)
        )
      }
      if (this.requestType === 'operation') {
        fetch('http://localhost:8094/baseR4/' + query.type, {
          method: 'POST',
          headers: {
            Accept: 'application/xml',
            'Content-Type': 'application/xml'
          },
          credentials: query.credentials,
          body: this.resource.xml
        })
      }
    })
  }
}
