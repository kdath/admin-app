export class PractitionerRole {
  constructor (object) {
    this.active = false // Aktiv
    this.json = this.template()
    this.practitionerAutorisationsId = ''
    this.organizationSOR = ''
    this.periodStart = ''
    this.periodEnd = ''
  }

  fromJson (json) {
    this.json = json
    // if (this.json.identifier) {
    //   this.id = (this.json.identifier[0].value) ? this.json.identifier[0].value : undefined
    // }
    this.active = (this.json.active) ? this.json.active : undefined
    if (this.json.period) {
      this.periodStart = (this.json.period.start) ? this.json.period.start : undefined
      this.periodEnd = (this.json.period.end) ? this.json.period.end : undefined
    }
    this.practitionerAutorisationsId = (this.json.practitioner.identifier.value) ? this.json.practitioner.identifier.value : undefined
    if (this.json.organization) {
      this.organizationSOR = (this.json.organization.identifier.value) ? this.json.organization.identifier.value : undefined
    }
  }

  updateJson () {
    this.json.period.start = this.periodStart
    this.json.practitioner.identifier.value = this.practitionerAutorisationsId
    this.json.organization.identifier.value = this.organizationSOR
  }

  template () {
    return {
      'resourceType': 'PractitionerRole',
      'identifier': [
        {
          'system': 'urn:ietf:rfc:3986',
          'value': '79bd3605-e5f7-49ff-b813-06befb9c036b'
        }
      ],
      'active': true,
      'period': {
        'start': '' // dags dato ved oprettelse
      },
      'practitioner': {
        'identifier': {
          'system': 'https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/',
          'value': '' // authID
        },
        'type': 'Practitioner'
      },
      'organization': {
        'identifier': {
          'system': 'urn:oid:1.2.208.176.1.1',
          'value': '' // SOR-kode
        },
        'type': 'Organization'
      }
    }
  }
}
