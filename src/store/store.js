// Model imports
import { bus } from '../main'
import { Organization } from '../common/model/organization'
import { Practitioner } from '../common/model/practitioner'
import { PractitionerRole } from '../common/model/practitionerRole'
import { ExecuteRequest } from '../common/model/executeRequest'
import { Parameters } from '../common/model/parameter'
import Vue from 'vue'
import Vuex from 'vuex'

// module imports
const FHIR = require('fhir.js/src/adapters/native')

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    firstSelected: {
      id: '',
      name: '',
      alias: ''
    },
    secondSelected: { id: '', name: '', alias: '' },
    thirdSelected: { id: '', name: '', alias: '' },
    changedItem: { id: '', name: '', alias: '' },
    organizationList: [],
    practitionerList: [],
    connectedList: [],
    addedPractitioners: [],
    executionQueue: [],
    changesMade: false,

    smartLocal: new FHIR({
      baseUrl: 'http://localhost:8094/baseR4',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }),
    smart: new FHIR({
      baseUrl: 'http://hapi.fhir.org/baseR4',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
  },
  getters: {
    organizationList (state) {
      return state.organizationList
    },
    practitionerList (state) {
      return state.practitionerList
    },
    connectedList (state) {
      return state.connectedList
    },
    executionQueue (state) {
      return state.executionQueue
    },
    changesMade (state) {
      return state.changesMade
    },
    addedPractitioners (state) {
      return state.addedPractitioners
    },

    firstSelected (state) {
      return state.firstSelected
    },
    secondSelected (state) {
      return state.secondSelected
    },
    thirdSelected (state) {
      return state.thirdSelected
    },
    changedItem (state) {
      return state.changedItem
    }
  },
  mutations: {
    organizationList (state, list) {
      state.organizationList = list
    },
    practitionerList (state, list) {
      state.practitionerList = list
    },
    connectedList (state, list) {
      state.connectedList = list
    },
    executionQueue (state, list) {
      state.executionQueue = list
    },
    changesMade (state, boolean) {
      state.changesMade = boolean
    },
    addedPractitioners (state, list) {
      state.addedPractitioners = list
    },

    firstSelected (state, selected) {
      state.firstSelected = selected
    },
    secondSelected (state, selected) {
      state.secondSelected = selected
    },
    thirdSelected (state, selected) {
      state.thirdSelected = selected
    },
    changedItem (state, selected) {
      state.changedItem = selected
    }
  },
  actions: {
    constructPractitionerRole (context, parameters) {
      const pracRole = new PractitionerRole()
      pracRole.active = true
      if (parameters.currentTab === 'Organization') {
        pracRole.organizationSOR = context.state.firstSelected.sor
        pracRole.practitionerAutorisationsId = context.state.secondSelected.authId
      } else {
        pracRole.organizationSOR = context.state.secondSelected.sor
        pracRole.practitionerAutorisationsId = context.state.firstSelected.authId
      }
      var date = new Date()
      var startDate = date.toISOString().slice(0, 10).replace(/-/g, '-')
      pracRole.periodStart = startDate
      pracRole.updateJson()

      const exeReq = new ExecuteRequest()
      exeReq.id = pracRole.organizationSOR + '-' + pracRole.practitionerAutorisationsId
      exeReq.requestType = 'create'
      pracRole.updateJson()
      exeReq.resource = pracRole
      exeReq.resourceType = 'PractitionerRole'

      let tempSelected = context.state.firstSelected
      tempSelected.listOfAdded.push(context.state.secondSelected)
      context.commit('firstSelected', tempSelected)

      context.dispatch('pushExecution', exeReq)
    },
    constructParameters (context, currentTab) {
      const parameters = new Parameters()
      if (currentTab === 'Organization') {
        parameters.organizationSOR = context.state.firstSelected.sor
        parameters.practitionerAutorisationsId = context.state.thirdSelected.authId
      } else {
        parameters.organizationSOR = context.state.thirdSelected.sor
        parameters.practitionerAutorisationsId = context.state.firstSelected.authId
      }
      parameters.updateJson()
      parameters.createXml()

      const exeReq = new ExecuteRequest()
      exeReq.id = parameters.organizationSOR + '-' + parameters.practitionerAutorisationsId

      exeReq.resource = parameters
      exeReq.resourceType = 'PractitionerRole/$removeConnection'
      exeReq.requestType = 'operation'

      let tempSelected = context.state.firstSelected
      tempSelected.listOfRemoved.push(context.state.thirdSelected)
      context.commit('firstSelected', tempSelected)

      context.dispatch('pushExecution', exeReq)
    },
    pushExecution (context, execution) {
      var dontPushToQueue = false
      var tempQueue = context.state.executionQueue
      for (let i = 0; i < tempQueue.length; i++) {
        if (tempQueue[i].id === execution.id) {
          if (execution.resourceType === 'Organization' && execution.requestType === 'update') {
            tempQueue.splice(i)
          } else if (tempQueue[i].resourceType === 'PractitionerRole' && tempQueue[i].requestType === 'create' && execution.requestType === 'operation') {
            dontPushToQueue = true
            tempQueue.splice(i)
            console.log(execution.id + ' is contained in executionQueue')
          } else if (execution.resourceType === 'Practitioner' && execution.requestType === 'operation' && tempQueue[i].requestType === 'create') {
            dontPushToQueue = true
            tempQueue.splice(i)
            console.log(execution.id + ' was deleted in executionqueue, before any create-request was sent')
          }
        }
      }
      if (!dontPushToQueue) {
        tempQueue.push(execution)
      }
      context.commit('changesMade', tempQueue.length > 0)
      if (execution.resourceType === 'Practitioner' && execution.requestType === 'create') {
        let tempList = context.state.addedPractitioners
        bus.$emit('firstSelectedChanged', execution.resource)
        tempList.push(execution.resource)
        context.commit('addedPractitioners', tempList)
      } else {
        context.state.firstSelected.setConnected()
        context.commit('connectedList', context.state.firstSelected.connected)
      }
      if (context.state.changesMade) {
        context.commit('changedItem', context.state.firstSelected)
      }
      context.commit('executionQueue', tempQueue)
    },

    executeSaved (context) {
      let tempSelected = context.state.firstSelected
      tempSelected.listOfAdded = []
      tempSelected.listOfRemoved = []
      tempSelected.hasInitializedConnected = false
      context.commit('firstSelected', tempSelected)
      return new Promise((resolve, reject) => {
        for (let i = 0; i < context.state.executionQueue.length; i++) {
          context.state.executionQueue[i].execute(context.state.smartLocal)
        }
        context.commit('executionQueue', [])
      })
    },

    getAllOrganizations (context) {
      const query = {
        type: 'Organization?_query=getAll',
        credentials: 'same-origin'
      }

      return new Promise((resolve, reject) => {
        context.state.smartLocal
          .search(query)
          .then(response => {
            const organizationList = []
            if (response.data.total > 0) {
              response.data.entry.forEach(organizationJson => {
                const organization = new Organization()
                organization.fromJson(organizationJson.resource)
                organizationList.push(organization)
              })
            }
            resolve(organizationList)
          })
          .catch(error => {
            console.log('error in getAllOrganizations')
            console.log(error)
            reject(error)
          })
      })
    },

    getConnected (context, selected) {
      const organizationQuery = {
        type: 'Practitioner?_query=getOrganizationPractitioners&identifier=urn:oid:1.2.208.176.1.1|' + selected.sor,
        credentials: 'same-origin'
      }
      const practitionerQuery = {
        type: 'Organization?_query=getPractitionerOrganizations&identifier=https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/|' + selected.authId,
        credentials: 'same-origin'
      }
      let query
      if (selected.sor !== undefined) {
        query = organizationQuery
      } else {
        query = practitionerQuery
      }
      return new Promise((resolve, reject) => {
        context.state.smartLocal
          .search(query)
          .then(response => {
            const returnList = []
            if (response.data.entry !== undefined) {
              response.data.entry.forEach(returnedJson => {
                let returned
                if (selected.sor === undefined) {
                  returned = new Organization()
                } else {
                  returned = new Practitioner()
                }
                returned.fromJson(returnedJson.resource)
                returnList.push(returned)
              })
            }
            selected.connected = returnList
            resolve(returnList)
          })
          .catch(error => {
            console.log('error in newFirstSelect')
            console.log(error)
            reject(error)
          })
      })
    },

    getAllPractitioners (context) {
      const query = {
        type: 'Practitioner?_query=getAll',
        credentials: 'same-origin',
        queryName: 'getAll'
      }
      return new Promise((resolve, reject) => {
        context.state.smartLocal
          .search(query)
          .then(response => {
            const practitionerList = []
            let count = response.data.total
            if (count > 0) {
              response.data.entry.forEach(practitionerJson => {
                const practitioner = new Practitioner()
                practitioner.fromJson(practitionerJson.resource)
                practitionerList.push(practitioner)
              })
            }
            resolve(practitionerList)
          })
          .catch(error => {
            console.log('error in getAllpractitioners')
            console.log(error)
            reject(error)
          })
      })
    },

    initializeStore (context) {
      context.dispatch('getAllOrganizations').then(list => {
        context.commit('organizationList', list)
      })
      context.dispatch('getAllPractitioners').then(list => {
        context.commit('practitionerList', list)
      })
    },

    newFirstSelected (context, selected) {
      if (selected.name !== '') {
        if (!selected.hasInitializedConnected) {
          let tempList = []
          context.dispatch('getConnected', selected).then(list => {
            if (list !== undefined) {
              for (let i = 0; i < list.length; i++) {
                tempList.push(list[i])
              }
            }
            selected.listOfRetrieved = tempList
            selected.setConnected()
            context.commit('connectedList', selected.connected)
            selected.hasInitializedConnected = true
          })
        } else {
          selected.setConnected()
          context.commit('connectedList', selected.connected)
        }
      }
    },

    saveChanges (context) {
      context.dispatch('executeSaved')
      context.state.changedItem.hasInitializedConnected = false
      context.state.changedItem.completeChanges()
      context.state.changesMade = false
      let tempPractitionerList = [...context.state.addedPractitioners,
        ...context.state.practitionerList]
      context.commit('practitionerList', tempPractitionerList)
      context.commit('addedPractitioners', [])
    },

    resetChanges (context) {
      context.state.changedItem.resetChanges()
      context.state.changesMade = false
      context.commit('firstSelected', context.state.changedItem)
      context.commit('addedPractitioners', [])
      context.commit('executionQueue', [])
      context.commit('connectedList', context.state.firstSelected.connected)
    }
  }
}
)
