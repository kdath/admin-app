import Vue from 'vue'
import Router from 'vue-router'
import organizationView from '../components/organizationView/organizationView.vue'
import practitionerView from '../components/practitionerView/practitionerView.vue'
import { store } from '../store/store.js'

Vue.use(Router)

export const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Organizations',
      component: organizationView,
      props: true
    },
    {
      path: '/medarbejdere',
      name: 'Practitioners',
      component: practitionerView,
      props: true
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (store.getters['changesMade']) {
    next(false)
  } else {
    next()
  }
})
